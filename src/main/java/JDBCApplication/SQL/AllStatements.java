package JDBCApplication.SQL;

public class AllStatements {
    static final public String SELECT_ALL_FROM_PROJECTS = "SELECT projectId, description AS project FROM projects";
    static final public String SELECT_ALL_FROM_EMPLOYEES = "SELECT employeeId, firstName, lastName, dateOfBirth FROM employees";
    static final public String SELECT_ALL_FROM_EMPLOYEES_WITH_DEPARTMENT_NAME = "SELECT employeeId, firstName, lastName, dateOfBirth,name FROM employees LEFT JOIN departments ON departments.departmentId = employees.departmentId";

    static final public String SELECT_ALL_FROM_DEPARTMENT = "SELECT * FROM departments";
    static final public String DELETE_FROM_DEPARTMENT_BY_DEPARTMENT_ID = "DELETE FROM departments WHERE departmentId=";
    static final public String INSERT_NEW_DEPARTMENT = "INSERT INTO departments (name) VALUES (?)";
    static final public String GET_ID_OF_NEW_DEPARTMENT = " SELECT LAST_INSERT_ID()";
    static final public String UPDATE_DEPARTMENT_NAME = "UPDATE departments set name=%s WHERE departmentId=%s";
    static final public String FIND_DEPARTMENT_BY_NAME = "SELECT departmentId,name FROM departments WHERE name=%s";
}
