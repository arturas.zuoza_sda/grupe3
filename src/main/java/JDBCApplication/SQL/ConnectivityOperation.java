package JDBCApplication.SQL;


import JDBCApplication.Hellpers.Text;
import JDBCApplication.Hellpers.Color;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Properties;

public class ConnectivityOperation extends Text {

    private static final Logger logger = (Logger) LogManager.getLogger(ConnectivityOperation.class);

    final static public String DB_PROPERTIES = "/database.properties";

    protected Properties properties = new Properties();
    protected Connection connection;

    public ConnectivityOperation() {
        super();
        try (final InputStream stream = this.getClass().getResourceAsStream( DB_PROPERTIES ) ) {
            properties.load(stream);

            System.out.println(properties);

        } catch ( IOException e) {
            //e.printStackTrace();
            logger.error(setColor(e.getMessage(), Color.RED));
        }

    }
}
