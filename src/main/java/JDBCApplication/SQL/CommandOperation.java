package JDBCApplication.SQL;


import JDBCApplication.Hellpers.Color;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class CommandOperation extends ConnectivityOperation{

    private static final Logger logger = (Logger) LogManager.getLogger(CommandOperation.class);

    private Statement statement = null;

    public CommandOperation(){
        super();

        String drivers = properties.getProperty("jdbc.drivers");
        if (drivers != null) System.setProperty("jdbc.drivers", drivers);

        String url = properties.getProperty("jdbc.url");
        String username = properties.getProperty("jdbc.username");
        String password = properties.getProperty("jdbc.password");

        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            statement = conn.createStatement();
            connection = conn;
        } catch (SQLException ex) {
            //ex.printStackTrace();
            logger.error(setColor(ex.getMessage(), Color.RED));
        }
    }
}
