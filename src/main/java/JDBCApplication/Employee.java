package JDBCApplication;

import JDBCApplication.Hellpers.Color;
import JDBCApplication.SQL.AllStatements;
import JDBCApplication.SQL.CommandOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;

import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Employee extends CommandOperation implements AutoCloseable {

    private static final Logger logger = (Logger) LogManager.getLogger(Employee.class);

    public Employee() {
        super();
    }

    public AutoCloseable queryOperation() throws SQLException {
        try {
            selectAllEmployee();
            selectAllEmployee("J");
            selectAllEmployeeWhereNotAssignedToDepartment();
            selectAllEmployeeWithDepartmentName();
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error(setColor(e.getMessage(), Color.RED));
        }
        return this;
    }

    private void selectAllEmployee() throws SQLException {

        try (final Statement statement = connection.createStatement(); final ResultSet resultSet = statement.executeQuery(AllStatements.SELECT_ALL_FROM_EMPLOYEES);) {

            if (null != connection) {

                System.out.println(setColor("------Select all from employees -------", Color.YELLOW));

                while (resultSet.next()) {

                    BigInteger asBigInteger = BigInteger.valueOf(resultSet.getLong("employeeId"));

                    String asFirstName = String.valueOf(resultSet.getString("firstName"));
                    String asLastName = String.valueOf(resultSet.getString("lastName"));
                    String asDateOfBirth = String.valueOf(resultSet.getDate("dateOfBirth"));

                    logger.info(setColor(String.format("On row # %s Employee ID is %s %s|%s|%s", resultSet.getRow(), asBigInteger, asFirstName, asLastName, asDateOfBirth), Color.YELLOW));

                }
                System.out.println(setColor("---------------------------------------", Color.YELLOW));
            }

        } catch (SQLException e) {
            logger.error(setColor(e.getMessage(), Color.RED));

        }
    }

    private void selectAllEmployee(String letter) throws SQLException {

        try (final Statement statement = connection.createStatement(); final ResultSet resultSet = statement.executeQuery(String.format("%S WHERE firstName Like %s%s%s ",AllStatements.SELECT_ALL_FROM_EMPLOYEES,"'%",letter,"%'"));) {

            if (null != connection) {

                System.out.println(setColor(String.format("------Select all from employees where firstName like %s -------",letter), Color.YELLOW));

                while (resultSet.next()) {

                    BigInteger asBigInteger = BigInteger.valueOf(resultSet.getLong("employeeId"));

                    String asFirstName = String.valueOf(resultSet.getString("firstName"));
                    String asLastName = String.valueOf(resultSet.getString("lastName"));
                    String asDateOfBirth = String.valueOf(resultSet.getDate("dateOfBirth"));

                    logger.info(setColor(String.format("On row # %s Employee ID is %s %s|%s|%s", resultSet.getRow(), asBigInteger, asFirstName, asLastName, asDateOfBirth), Color.YELLOW));

                }
                System.out.println(setColor("--------------------------------------------------------------", Color.YELLOW));
            }

        } catch (SQLException e) {
            logger.error(setColor(e.getMessage(), Color.RED));

        }
    }

    private void selectAllEmployeeWhereNotAssignedToDepartment() throws SQLException {

        try (final Statement statement = connection.createStatement(); final ResultSet resultSet = statement.executeQuery(String.format("%S WHERE departmentId is NULL",AllStatements.SELECT_ALL_FROM_EMPLOYEES));) {

            if (null != connection) {

                System.out.println(setColor(String.format("------Select all from employees where employee not assigned to department -------"), Color.YELLOW));

                while (resultSet.next()) {

                    BigInteger asBigInteger = BigInteger.valueOf(resultSet.getLong("employeeId"));

                    String asFirstName = String.valueOf(resultSet.getString("firstName"));
                    String asLastName = String.valueOf(resultSet.getString("lastName"));
                    String asDateOfBirth = String.valueOf(resultSet.getDate("dateOfBirth"));

                    logger.info(setColor(String.format("On row # %s Employee ID is %s %s|%s|%s", resultSet.getRow(), asBigInteger, asFirstName, asLastName, asDateOfBirth), Color.YELLOW));

                }
                System.out.println(setColor("---------------------------------------------------------------------------------", Color.YELLOW));
            }

        } catch (SQLException e) {
            logger.error(setColor(e.getMessage(), Color.RED));

        }
    }

    private void selectAllEmployeeWithDepartmentName() throws SQLException {

        try (final Statement statement = connection.createStatement(); final ResultSet resultSet = statement.executeQuery(AllStatements.SELECT_ALL_FROM_EMPLOYEES_WITH_DEPARTMENT_NAME);) {

            if (null != connection) {

                System.out.println(setColor("------Select all from employees with department name-------", Color.YELLOW));

                while (resultSet.next()) {

                    BigInteger asBigInteger = BigInteger.valueOf(resultSet.getLong("employeeId"));

                    String asFirstName = String.valueOf(resultSet.getString("firstName"));
                    String asLastName = String.valueOf(resultSet.getString("lastName"));
                    String asDateOfBirth = String.valueOf(resultSet.getDate("dateOfBirth"));
                    String asDepartmentName = String.valueOf(resultSet.getString("name"));
                    logger.info(setColor(String.format("On row # %s Employee ID is %s %s|%s|%s|%s", resultSet.getRow(), asBigInteger, asFirstName, asLastName, asDateOfBirth,asDepartmentName), Color.YELLOW));

                }
                System.out.println(setColor("-----------------------------------------------------------", Color.YELLOW));
            }

        } catch (SQLException e) {
            logger.error(setColor(e.getMessage(), Color.RED));

        }
    }

    @Override
    public void close() throws Exception {

        if (null != super.connection) {
            connection.close();
            logger.warn(setColor("OK, closed DB Connection in {}", Color.BLUE), this.getClass());
        }

    }
}
