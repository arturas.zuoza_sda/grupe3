package JDBCApplication.Hellpers;

public class Text {
    Colors colors = new Colors();
    public String setColor(String text,Color color){
        switch (color){
            case GREEN:
                return String.format("%s %s %s" ,colors.GREEN,text, colors.WHITE);
            case YELLOW:
                return String.format("%s %s %s" ,colors.YELLOW,text,colors.WHITE);
            case RED:
                return String.format("%s %s %s" ,colors.RED,text,colors.WHITE);
            case BLUE:
                return String.format("%s %s %s" ,colors.BLUE,text,colors.WHITE);
            default:
                return text;
        }
    }
}
