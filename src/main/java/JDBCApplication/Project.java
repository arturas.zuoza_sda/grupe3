package JDBCApplication;

import JDBCApplication.Hellpers.Color;
import JDBCApplication.SQL.AllStatements;
import JDBCApplication.SQL.CommandOperation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;

import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Project extends CommandOperation implements AutoCloseable {

    private static final Logger logger = (Logger) LogManager.getLogger(Employee.class);

    public AutoCloseable queryOperation() throws SQLException {
        try {
            selectAll();

        } catch (SQLException e) {
            e.printStackTrace();
            logger.error(setColor(e.getMessage(), Color.RED) );
        }
        return this;
    }

    private void selectAll()throws SQLException{

        try(final Statement statement = connection.createStatement(); final ResultSet resultSet =  statement.executeQuery(AllStatements.SELECT_ALL_FROM_PROJECTS);){

            if (null!=connection){

                System.out.println(setColor("------Select all from projects -------",Color.YELLOW));

                while(resultSet.next()){

                    BigInteger asBigInteger = BigInteger.valueOf( resultSet.getLong("projectId") );

                    String asProject = String.valueOf(resultSet.getString("project"));

                    logger.info(setColor(String.format("On row # %s Project ID is %s",resultSet.getRow(),asBigInteger,asProject),Color.YELLOW));

                }
                System.out.println(setColor("---------------------------------------",Color.YELLOW));
            }

        } catch (SQLException e) {
            e.printStackTrace();
            logger.error( setColor(e.getMessage() ,Color.RED) );

        }
    }

    @Override
    public void close() throws Exception {

        if (null!= super.connection){
            connection.close();
            logger.warn(setColor("OK, closed DB Connection in {}", Color.BLUE),this.getClass());
        }

    }
}
