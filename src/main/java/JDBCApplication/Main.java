package JDBCApplication;


import JDBCApplication.Hellpers.Text;
import JDBCApplication.Hellpers.Color;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;

public class Main {

    private static final Logger logger = (Logger) LogManager.getLogger(Main.class);

    public static void main(String[] args) {
//        1. Display all projects (projectId, description)
//        2. Display all employees (employeeId, firstName, lastName, dateOfBirth)
//        3. Display all employees with names starting with the letter J (employeeId, firstName, lastName,
//                dateOfBirth)
//        4. Display all employees that haven’t been assigned to a department
//        5. Display all employees along with the department they’re in (employeeId, firstName, lastName,
//                dateOfBirth, departmentName)

        Text text = new Text();

        logger.info(text.setColor("Starting Main class", Color.GREEN));
        try(AutoCloseable runProject = new Project().queryOperation(); AutoCloseable runEmployee = new Employee().queryOperation()){
            logger.info(text.setColor("So exit happily now, with all the resources auto-closed.",Color.BLUE));
        } catch(Exception e) {
            e.printStackTrace();
            logger.error(text.setColor(e.getMessage(),Color.RED));
        } finally {
            logger.warn(text.setColor("Finished",Color.GREEN));
        }

    }
}
