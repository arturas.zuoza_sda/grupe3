package HibernateApplication;

import HibernateApplication.Entity.ProjectRepository;

public class Main {
    public static void main(String[] args) {
//        1. Create class Employee and map it to the Employees table
//        2. Create class Project and map it to the Projects table
//        3. Create class ProjectRepository
//        a. Implement findById - takes an Integer parameter and returns the Project corresponding to
//        the provided projectId
//        b. Implement save - takes a Project object parameter and saves the provided Project in the
//                database
//        c. Implement delete - takes an existing Project object parameter(which was previously
//                retrieved from the database) and deletes it from the database
//        d. Implement update - takes an existing Project object parameter(which was previously
//                retrieved from the database and had its name changed) and updates its name in the
//        database
//        4. Optional: Create repositories for Employee and Department

         try{
             ProjectRepository project = new  ProjectRepository().queryOperation();
         }catch (Exception e){
             e.printStackTrace();

         }
    }
}
