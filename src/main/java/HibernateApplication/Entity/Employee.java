package HibernateApplication.Entity;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Employees")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer employeeId;

    @Column(name = "firstName")
    private String firstName;

    @Column(name = "lastName")
    private String lastName;

    @Column(name = "dateOfBirth")
    private Date dateOfBirth;

    @Column(name = "phoneNumber")
    private String phoneNumber;

    @Column(name = "email")
    private String email;

    @Column(name = "salary")
    private Integer salary;

    public String getFirstName() {
        return firstName;
    }

    public Employee setFirstName(final String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public Employee setLastName(final String lastName) {
        this.lastName = lastName;
        return this;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public Employee setDateOfBirth(final Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Employee setPhoneNumber(final String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Employee setEmail(final String email) {
        this.email = email;
        return this;
    }

    public Integer getSalary() {
        return salary;
    }

    public Employee setSalary(final Integer salary) {
        this.salary = salary;
        return this;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "employeeId->" + employeeId +
                ", firstName->'" + firstName + '\'' +
                ", lastName->'" + lastName + '\'' +
                ", dateOfBirth->" + dateOfBirth +
                ", phoneNumber->'" + phoneNumber + '\'' +
                ", email->'" + email + '\'' +
                ", salary->" + salary +
                '}';
    }
}
