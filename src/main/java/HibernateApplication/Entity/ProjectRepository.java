package HibernateApplication.Entity;

import HibernateApplication.Utilities.HibernateUtilities;
import JDBCApplication.Hellpers.Color;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import org.hibernate.Session;


import java.sql.SQLException;

public class ProjectRepository extends HibernateUtilities implements IProjectCommands {
    Logger logger = (Logger) LogManager.getLogger(ProjectRepository.class);

    public ProjectRepository queryOperation() throws SQLException {
        try{
            Project project = new Project();
            project.setDescription( "C# Project");
            System.out.println(setColor(String.format("--------------- Creating new Project->%s ---------------",project.getDescription()),Color.YELLOW));
            save(project);
            System.out.println(setColor("-------------------------------------------------------------------",Color.YELLOW));
        }catch (Exception e){
            e.printStackTrace();
            logger.error(setColor(e.getMessage(),Color.RED));
        }
        return this;
    }
    @Override
    public Project findById(Integer id) {
        return null;
    }

    @Override
    public void save(Project project) {
        transaction = null;
        try{
            Session session = HibernateUtilities.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.persist(project);
            transaction.commit();
            logger.info(setColor(String.format("New project->%s was created",project.getDescription()), Color.YELLOW));
        }catch (Exception e){
            e.printStackTrace();
            transaction.rollback();
            logger.error(setColor(e.getMessage(), Color.RED));
        }
    }

    @Override
    public void delete(Project project) {

    }

    @Override
    public void update(Project project) {

    }
}
