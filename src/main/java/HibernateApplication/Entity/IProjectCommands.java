package HibernateApplication.Entity;

public interface IProjectCommands {
    Project findById(final Integer id);
    void save(Project project);
    void delete(Project project);
    void update(Project project);
}
