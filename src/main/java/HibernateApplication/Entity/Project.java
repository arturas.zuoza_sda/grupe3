package HibernateApplication.Entity;

import javax.persistence.*;

@Entity
@Table(name = "projects")
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer projectId;

    @Column(name = "description")
    private String description;

    public Project() {
    }

    public Integer getProjectId() {
        return this.projectId;
    }

    public void setProjectId(final Integer projectId) { this.projectId =  projectId;}

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Project{" +
                "projectId->" + projectId +
                ", description->'" + description + '\'' +
                '}';
    }
}
