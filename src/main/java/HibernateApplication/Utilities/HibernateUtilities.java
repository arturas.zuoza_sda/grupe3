package HibernateApplication.Utilities;

import HibernateApplication.Entity.Employee;
import HibernateApplication.Entity.Project;

import JDBCApplication.Hellpers.Color;
import JDBCApplication.Hellpers.Text;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;



public class HibernateUtilities extends Text {
    private static Logger logger = (Logger) LogManager.getLogger(HibernateUtilities.class);
    private static SessionFactory sessionFactory;
    protected Transaction transaction;

    public static SessionFactory getSessionFactory(){
        if (sessionFactory==null){
            try{
                Configuration configuration = new Configuration();
                configuration.configure("/hibernate.cfg.xml");
                sessionFactory = configuration.buildSessionFactory();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return sessionFactory;
    }


}
