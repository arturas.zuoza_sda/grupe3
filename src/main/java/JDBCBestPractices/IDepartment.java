package JDBCBestPractices;

import java.sql.SQLException;
import java.util.List;

public interface IDepartment {

    List<Department> findAll() throws SQLException;

    Department findById(final int departmentId) throws SQLException;

    void deleteById(Department department)throws SQLException;

    Department save(Department department) throws SQLException ;

    void update(Department department) throws SQLException ;

    List<Department> findByName(final String name) throws SQLException;

}
