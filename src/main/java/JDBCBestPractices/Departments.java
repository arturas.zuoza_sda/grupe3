package JDBCBestPractices;

public enum Departments {
    HR(1),
    Finance(2),
    Dev(3),
    DEVOPS("Devops");
    private int id;
    private String name;
    private Departments(final String name){
        this.name = name;
    };
    private  Departments(final int id){
        this.id = id;
    }
    public int  getId(){
        return id;
    }
    public String  getName(){
        return name;
    }
}
