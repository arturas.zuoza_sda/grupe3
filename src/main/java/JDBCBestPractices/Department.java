package JDBCBestPractices;

public class Department {

    private final Integer departmentId;
    private final String name;

    private Department(DepartmentBuilder departmentBuilder) {
        this.departmentId = departmentBuilder.departmentId;
        this.name = departmentBuilder.name;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString(){
        return (!name.equals("default")?String.format("Department ID->%s | Department Name->%s",departmentId,name):"");
    }
    public static class DepartmentBuilder{

        private  Integer departmentId;
        private  String name;

        public DepartmentBuilder setDepartmentId(Integer departmentId) {
            this.departmentId = departmentId;
            return this;
        }

        public DepartmentBuilder setName(String name) {
            this.name = name;
            return this;
        }
         public Department build(){
            return new Department(this);
         }
    }
}
