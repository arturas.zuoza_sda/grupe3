package JDBCBestPractices;

import JDBCApplication.Hellpers.Color;
import JDBCApplication.Hellpers.Text;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;

public class Main {

    private static final Logger logger = (Logger) LogManager.getLogger(Main.class);

    public static void main(String[] args) {

//        1. Create class Department mapping the departments table: Integer departmentId, String name
//        2. Create class DepartmentRepository
//        a. Implement findAll() - returns a list of Department objects representing all table rows
//        b. Implement findById(Integer departmentId) - takes an Integer parameter and returns the
//        Department object corresponding to the provided departmentId
//        c. Implement deleteById(Integer departmentId) - takes an Integer parameter and deletes the
//        table row corresponding to the provided employeeId PS. (NAUDOSIU DEPARTMENT OBJEKTA KAD ISVESTI DEPARTAMENTO PAVADINIMA SU ID I LOGGERI)

//        d. Implement save (Department department) - takes a new Department object parameter
//        with no departmentId and saves the provided department in the database
//        e. Implement update(Department department) - takes an existing Department object
//        parameter (which already has a departmentId that exists in the database) and for the
//        provided departmentId, it updates the name property in the database
//        f. Implement findByName(String name) - takes a String parameter and returns a list of
//        Department objects representing all table rows that have the provided name
//        3. Optional: Implement ProjectRepository and EmployeeRepository methods findAll, findById,
//                deleteById, save, update

        Text text = new Text();
        logger.info(text.setColor("Starting in Main", Color.GREEN));

        try(AutoCloseable runDepartment = new DepartmentRepository().queryOperation()){
            logger.info(text.setColor("So exit happily now, with all the resources auto-closed.",Color.BLUE));
        }catch (Exception e){
            e.printStackTrace();
            logger.error(text.setColor(e.getMessage(),Color.RED));
        }finally {
            logger.warn(text.setColor("Finished",Color.GREEN));
        }
    }
}
