package JDBCBestPractices;

import JDBCApplication.Hellpers.Color;
import JDBCApplication.SQL.AllStatements;
import JDBCApplication.SQL.CommandOperation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;


import java.sql.PreparedStatement;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class DepartmentRepository extends CommandOperation implements IDepartment, AutoCloseable {

    private static final Logger logger = (Logger) LogManager.getLogger(DepartmentRepository.class);

    public DepartmentRepository() {
        super();
    }

    public AutoCloseable queryOperation() throws SQLException {

        try {
            System.out.println(setColor("---------------------Select all from departments------------------", Color.YELLOW));
            findAll().forEach(department -> logger.info(setColor(String.format("Department Id->%s Department Name->%s", department.getDepartmentId(), department.getName()), Color.YELLOW)));
            System.out.println(setColor("------------------------------------------------------------------", Color.YELLOW));
            int departmentId = Departments.Finance.getId();
            Department department = findById(departmentId);
            System.out.println(setColor(String.format("---------------------Select all from departments where department Id is %s ------------------", departmentId), Color.YELLOW));
            logger.info(setColor(String.format("Department Id->%s Department Name->%s", department.getDepartmentId(), department.getName()), Color.YELLOW));
            System.out.println(setColor("--------------------------------------------------------------------------------------------", Color.YELLOW));
            Department department1 = new Department.DepartmentBuilder().setName(Departments.DEVOPS.getName()).setDepartmentId(0).build();
            System.out.println(setColor(String.format("--------------------- Save new department %s in departments ------------------", department1.getName()), Color.YELLOW));
            Department department2 = save(department1);
            System.out.println(setColor("----------------------------------------------------------------------------------", Color.YELLOW));
            String oldDepartmentName = department1.getName();
            Department department3 = new Department.DepartmentBuilder().setName("Seniors").setDepartmentId(department2.getDepartmentId()).build();
            System.out.println(setColor(String.format("--------------------- Update department %s in departments set new department name->%s------------------", oldDepartmentName, department3.getName()), Color.YELLOW));
            update(department3);
            System.out.println(setColor("----------------------------------------------------------------------------------------------------------------", Color.YELLOW));
            System.out.println(setColor(String.format("---------------------Find department %s by name ------------------", department3.getName()), Color.YELLOW));
            findByName(department3.getName()).forEach(d -> logger.info(setColor(String.format("Found Department->%s with ID->%s", d.getName(), d.getDepartmentId()), Color.YELLOW)));
            System.out.println(setColor("--------------------------------------------------------------------------------------------", Color.YELLOW));
            System.out.println(setColor(String.format("---------------------Delete department %s from departments where department Id is %s ------------------", department3.getName(), department3.getDepartmentId()), Color.YELLOW));
            deleteById(department3);
            System.out.println(setColor("---------------------------------------------------------------------------------------------------------------", Color.YELLOW));
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error(setColor(e.getMessage(),Color.RED));
        }
        return this;
    }

    @Override
    public List<Department> findAll() throws SQLException {
        List<Department> departments = new ArrayList<Department>();
        try (final Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(AllStatements.SELECT_ALL_FROM_DEPARTMENT);) {

            if (connection != null) {

                while (resultSet.next()) {

                    departments.add(new Department.DepartmentBuilder().setDepartmentId(resultSet.getInt("departmentId")).setName(resultSet.getString("name")).build());
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
            logger.error(setColor(e.getMessage(), Color.RED));
        }
        return departments;
    }

    @Override
    public Department findById(final int departmentId) throws SQLException {
        Department department = new Department.DepartmentBuilder().setDepartmentId(0).setName("default").build();
        try (final Statement statement = connection.createStatement(); final ResultSet resultSet = statement.executeQuery(String.format("%s WHERE departmentId=%s", AllStatements.SELECT_ALL_FROM_DEPARTMENT, departmentId));) {
            if (null != connection) {
                while (resultSet.next()) {
                    department = new Department.DepartmentBuilder().setDepartmentId(resultSet.getInt("departmentId")).setName(resultSet.getString("name")).build();
                }
            }
        }
        return department;
    }

    @Override
    public void deleteById(Department department) throws SQLException {
        if (connection != null) {
            connection.setAutoCommit(false);
            try {
                PreparedStatement preparedStatement = connection.prepareStatement(String.format("%s%s", AllStatements.DELETE_FROM_DEPARTMENT_BY_DEPARTMENT_ID, department.getDepartmentId()));
                preparedStatement.executeUpdate();
                logger.info(setColor(String.format("Department %s with Id-> %s is successfully deleted", department.getName(), department.getDepartmentId()), Color.YELLOW));
                connection.commit();
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error(setColor(e.getMessage(), Color.RED));
                connection.rollback();
            }
        }
    }

    @Override
    public Department save(Department department) throws SQLException {
        Department department1 = department;
        if (connection != null) {
            connection.setAutoCommit(false);
            try (Statement statement = connection.createStatement()) {
                PreparedStatement preparedStatement = connection.prepareStatement(AllStatements.INSERT_NEW_DEPARTMENT);
                preparedStatement.setString(1, department.getName());
                preparedStatement.executeUpdate();
                connection.commit();
                ResultSet resultSet = statement.executeQuery(AllStatements.GET_ID_OF_NEW_DEPARTMENT);
                while (resultSet.next()) {
                    department1 = new Department.DepartmentBuilder().setName(department.getName()).setDepartmentId(resultSet.getInt("LAST_INSERT_ID()")).build();
                }
                logger.info(setColor(String.format("New department->%s is added new department ID->%s", department1.getName(), department1.getDepartmentId()), Color.YELLOW));
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error(setColor(e.getMessage(), Color.RED));
                connection.rollback();
            }
        }
        return department1;
    }

    @Override
    public void update(Department department) throws SQLException {
        if (connection != null) {
            connection.setAutoCommit(false);
            try {
                PreparedStatement preparedStatement = connection.prepareStatement(String.format(AllStatements.UPDATE_DEPARTMENT_NAME, "'" + department.getName() + "'", department.getDepartmentId()));
                preparedStatement.executeUpdate();
                connection.commit();
                logger.info(setColor(String.format("Department with Id-> %s is successfully updated department new name->%s", department.getDepartmentId(), department.getName()), Color.YELLOW));
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error(setColor(e.getMessage(), Color.RED));
                connection.rollback();
            }
            connection.commit();
        }
    }

    @Override
    public List<Department> findByName(String name) throws SQLException {
        List<Department> departments = new ArrayList<Department>();
        try (final Statement statement = connection.createStatement(); final ResultSet resultSet = statement.executeQuery(String.format(AllStatements.FIND_DEPARTMENT_BY_NAME, "'" + name + "'"));) {
            if (null != connection) {
                while (resultSet.next()) {
                    departments.add(new Department.DepartmentBuilder().setDepartmentId(resultSet.getInt("departmentId")).setName(resultSet.getString("name")).build());
                }
            }
        }
        return departments;
    }

    @Override
    public void close() throws Exception {
        if (null != super.connection) {
            connection.close();
            logger.warn(setColor("OK, closed DB Connection in {}", Color.BLUE), this.getClass());
        }

    }
}
